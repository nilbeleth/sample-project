/*****************************************************************************\
 * Project:   sample-project
 * Copyright: (c) 2018 by Matej Odalos <nilbeleth@valec.net>
 * License:   MIT, see the LICENSE file for more details
 *
 * Simple Hello world!!! application.
 *
\*****************************************************************************/
#include "main.h"
#include "version.h"

#include <iostream>
#include <cstdlib>

using namespace std;


int main(int argc, char const* argv[])
{
  (void) argc;
  (void) argv;
  cout << "Hello world!" << endl;

  return EXIT_SUCCESS;
}
